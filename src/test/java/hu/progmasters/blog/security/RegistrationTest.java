package hu.progmasters.blog.security;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
class RegistrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_registrationSuccesful() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"TestKata\",\n" +
                "    \"password\": \"ValidPassword123\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isCreated());
    }

    @Test
    void test_registrationUsernameEmpty() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"\",\n" +
                "    \"password\": \"ValidPassword123\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("username")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Username must be at least 3 characters long")));
    }

    @Test
    void test_registrationUsernameTooShort() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"ab\",\n" +
                "    \"password\": \"ValidPassword123\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("username")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Username must be at least 3 characters long")));
    }

    @Test
    void test_registrationUsernameContainsWhitespace() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"abc def\",\n" +
                "    \"password\": \"ValidPassword123\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("username")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Username cannot contain whitespaces")));
    }
    @Test
    void test_registrationPasswordEmpty() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"ValidUsername\",\n" +
                "    \"password\": \"\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("password")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Password must be at least 8 characters long")));
    }

    @Test
    void test_registrationPasswordTooShort() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"ValidUsername\",\n" +
                "    \"password\": \"short\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("password")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Password must be at least 8 characters long")));
    }

    @Test
    void test_registrationPasswordContainsWhitespace() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"ValidUsername\",\n" +
                "    \"password\": \"Invalid Pass\",\n" +
                "    \"email\": \"test@example.com\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("password")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Password cannot contain whitespaces")));
    }

    @Test
    void test_registrationEmailEmpty() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"ValidUsername\",\n" +
                "    \"password\": \"ValidPassword123\",\n" +
                "    \"email\": \"\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("email")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Email cannot be empty")));
    }

    @Test
    void test_registrationInvalidEmailFormat() throws Exception {
        String inputCommand = "{\n" +
                "    \"username\": \"ValidUsername\",\n" +
                "    \"password\": \"ValidPassword123\",\n" +
                "    \"email\": \"invalidEmail\"\n" +
                "}";

        mockMvc.perform(post("/api/public/user/registration")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("email")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Invalid email format")));
    }

}

