package hu.progmasters.blog.repository;

import hu.progmasters.blog.domain.Comment;
import hu.progmasters.blog.domain.Post;
import hu.progmasters.blog.dto.comment.CommentDetails;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void test_findAllCommentByPostId(){
        TestPost();

        List<CommentDetails> result = commentRepository.findAllCommentByPostId(1L);

        Comment comment = entityManager.find(Comment.class,1L);
        Comment comment2 = entityManager.find(Comment.class,2L);
        Comment comment3 = entityManager.find(Comment.class,3L);
        assertTrue(result.contains(modelMapper.map(comment,CommentDetails.class)));
        assertTrue(result.contains(modelMapper.map(comment2,CommentDetails.class)));
        assertTrue(result.contains(modelMapper.map(comment3,CommentDetails.class)));
    }
    @Test
    public void test_findAllCommentByPostIdOtherPostCommentAreNotDisplayed(){
        TestPost();

        List<CommentDetails> result = commentRepository.findAllCommentByPostId(1L);

        Comment comment = entityManager.find(Comment.class,1L);
        Comment comment2 = entityManager.find(Comment.class,2L);
        Comment comment3 = entityManager.find(Comment.class,3L);
        Comment comment4 = entityManager.find(Comment.class,4L);
        assertTrue(result.contains(modelMapper.map(comment,CommentDetails.class)));
        assertTrue(result.contains(modelMapper.map(comment2,CommentDetails.class)));
        assertTrue(result.contains(modelMapper.map(comment3,CommentDetails.class)));

        assertFalse(result.contains(modelMapper.map(comment4,CommentDetails.class)));
    }
    private void TestPost() {
        entityManager.createNativeQuery(
                "INSERT INTO category (category_name) VALUES ('Test Category 1'); " +
                        "INSERT INTO post (title, post_Body, img_Url, category_id, deleted, scheduled) VALUES ('New Post', 'Content','new URL',1 , false, false); " +
                        "INSERT INTO post (title, post_Body, img_Url, category_id, deleted, scheduled) VALUES ('New Post2', 'Content2','new URL2',1 , false, false); " +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (1, 'Author','comment');" +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (1, 'Author','comment2');" +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (1, 'Author','comment3');" +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (2, 'Author','comment4');"
        ).executeUpdate();
    }
}
