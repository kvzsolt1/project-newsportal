package hu.progmasters.blog.repository;

import hu.progmasters.blog.domain.Post;
import hu.progmasters.blog.domain.PostCategory;
import hu.progmasters.blog.domain.PostTag;
import hu.progmasters.blog.dto.tags.PostTagListItem;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TagsRepositoryTest {

    @Autowired
    private TagsRepository tagsRepository;

    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void testFindAllOrderBy() {
        PostTag tag = new PostTag();
        PostTag tag2 = new PostTag();
        PostTag tag3 = new PostTag();
        PostTagListItem postTagListItem = modelMapper.map(tag,PostTagListItem.class);
        PostTagListItem postTagListItem2 = modelMapper.map(tag2,PostTagListItem.class);
        PostTagListItem postTagListItem3 = modelMapper.map(tag3,PostTagListItem.class);

        entityManager.persist(tag);
        entityManager.persist(tag2);
        entityManager.persist(tag3);

        entityManager.flush();

        List<PostTagListItem> result = tagsRepository.findAllOrderBy();

        assertTrue(result.contains(postTagListItem));
        assertTrue(result.contains(postTagListItem2));
        assertTrue(result.contains(postTagListItem3));
    }

    @Test
    public void testFindByTagByNameByPostId() {
        TestPost();

        Optional<PostTag> result = tagsRepository.findByTagByNameByPostId("New tag", 1L);
        PostTag tags = entityManager.find(PostTag.class,1L);
        assertTrue(result.get().getTagsName().equals(tags.getTagsName()));
    }

    private void TestPost() {
        entityManager.createNativeQuery(
                "INSERT INTO category (category_name) VALUES ('Test Category 1'); " +
                        "INSERT INTO post (title, post_Body, img_Url, category_id, deleted, scheduled) VALUES ('New Post', 'Content','new URL',1,false,false); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag'); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag2'); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag3'); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag4'); " +
                        "INSERT INTO post_post_tag ( post_tag_id, post_id ) VALUES (1,1); "
        ).executeUpdate();
    }
}
