package hu.progmasters.blog.service;

import hu.progmasters.blog.domain.Comment;
import hu.progmasters.blog.dto.comment.CommentDetails;
import hu.progmasters.blog.dto.comment.CommentEditFormData;
import hu.progmasters.blog.dto.comment.CommentFormData;
import hu.progmasters.blog.exception.NotFoundCommentException;
import org.assertj.core.error.ShouldExist;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class CommentServiceTest {

    @Autowired
    private CommentService commentService;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ModelMapper modelMapper;
    @Test
    void test_createComment() {
        TestPost();
        CommentFormData commentFormData = new CommentFormData(1L,"Lajos","CommentBody");
        commentService.createComment(commentFormData);
        Comment comment = commentService.findCommentById(5L);

        assertEquals("Lajos", comment.getAuthor());
        assertEquals("CommentBody", comment.getCommentBody());
    }
    @Test
    void test_findCommentById(){
        TestPost();
        Comment comment = commentService.findCommentById(1L);
        assertEquals("Author", comment.getAuthor());
        assertEquals("comment", comment.getCommentBody());
    }
    @Test
    void test_editComment() {
        TestPost();
        Comment comment = commentService.findCommentById(1L);
        assertEquals("Author", comment.getAuthor());
        assertEquals("comment", comment.getCommentBody());

        CommentEditFormData commentFormData = new CommentEditFormData("edited author","edited commentBody");
        commentService.editComment(1L,commentFormData);

        assertEquals("edited author", comment.getAuthor());
        assertEquals("edited commentBody", comment.getCommentBody());
    }

    @Test
    void test_deleteComment() {
        TestPost();
        Comment comment = commentService.findCommentById(1L);
        assertTrue(comment.getAuthor().equals("Author"));
        commentService.deleteComment(1L);
        assertThrows(NotFoundCommentException.class,()->commentService.findCommentById(1L) );
    }
    @Test
    void test_getCommentsList(){
        TestPost();
        List<CommentDetails> result = commentService.getCommentsList(1L);
        Comment comment = entityManager.find(Comment.class,1L);
        Comment comment2 = entityManager.find(Comment.class,2L);
        Comment comment3 = entityManager.find(Comment.class,3L);
        assertTrue(result.contains(modelMapper.map(comment,CommentDetails.class)));
        assertTrue(result.contains(modelMapper.map(comment2,CommentDetails.class)));
        assertTrue(result.contains(modelMapper.map(comment3,CommentDetails.class)));
    }

    private void TestPost() {
        entityManager.createNativeQuery(
                "INSERT INTO category (category_name) VALUES ('Test Category 1'); " +
                        "INSERT INTO post (title, post_Body, img_Url, category_id, deleted, scheduled) VALUES ('New Post', 'Content','new URL',1 , false, false); " +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (1, 'Author','comment');" +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (1, 'Author','comment2');" +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (1, 'Author','comment3');" +
                        "INSERT INTO comment (post_Id, author, comment_Body) VALUES (1, 'Author','comment4');"
        ).executeUpdate();
    }

}
