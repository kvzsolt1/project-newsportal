package hu.progmasters.blog.service;

import hu.progmasters.blog.domain.Comment;
import hu.progmasters.blog.domain.PostTag;
import hu.progmasters.blog.dto.comment.CommentDetails;
import hu.progmasters.blog.dto.comment.CommentEditFormData;
import hu.progmasters.blog.dto.comment.CommentFormData;
import hu.progmasters.blog.dto.post.PostDetails;
import hu.progmasters.blog.dto.tags.PostTagListItem;
import hu.progmasters.blog.dto.tags.TagsDetails;
import hu.progmasters.blog.dto.tags.TagsFromData;
import hu.progmasters.blog.exception.NotFoundPostTagException;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class TagsServiceTest {

    @Autowired
    private TagsService tagsService;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ModelMapper modelMapper;

    @Test
    void test_createTags() {
        TestPost();
        TagsFromData[] tagsFromData = new TagsFromData[]{new TagsFromData("Test tag")};
        tagsService.createTags(1L,tagsFromData);
        PostTag postTag = tagsService.findPostTagById(4L);

        assertEquals("Test tag", postTag.getTagsName());
    }

    @Test
    void test_deleteTags(){
        TestPost();
        assertTrue(tagsService.findPostTagById(1L) != null);
        tagsService.deleteTags(1L);
        assertThrows(NotFoundPostTagException.class,()->tagsService.findPostTagById(1L));
    }

    @Test
    void test_tagsDetails(){
        TestPost();
        PostTag postTag = tagsService.findPostTagById(1L);
        TagsDetails tagsDetails = modelMapper.map(postTag,TagsDetails.class);
        assertTrue(postTag.getTagsName().equals(tagsDetails.getTagsName()));
    }

    @Test
    void test_editComment() {
        TestPost();
        PostTag postTag = tagsService.findPostTagById(1L);
        assertEquals("New tag", postTag.getTagsName());

        TagsFromData tagsFromData = new TagsFromData("Test tag");
        tagsService.editingTag(1L,tagsFromData);

        assertEquals("Test tag", postTag.getTagsName());
    }

    @Test
    void test_findPostTagById(){
        TestPost();
        PostTag postTag = tagsService.findPostTagById(1L);
        assertEquals("New tag", postTag.getTagsName());
    }

    @Test
    void test_getPostTagListItems(){
        TestPost();
        List<PostTagListItem> result = tagsService.getPostTagListItems();
        PostTag postTag = entityManager.find(PostTag.class,1L);
        PostTag postTag2 = entityManager.find(PostTag.class,2L);
        PostTag postTag3 = entityManager.find(PostTag.class,3L);

        assertTrue(result.contains(modelMapper.map(postTag,PostTagListItem.class)));
        assertTrue(result.contains(modelMapper.map(postTag2,PostTagListItem.class)));
        assertTrue(result.contains(modelMapper.map(postTag3,PostTagListItem.class)));

    }

    private void TestPost() {
        entityManager.createNativeQuery(
                "INSERT INTO category (category_name) VALUES ('Test Category 1'); " +
                        "INSERT INTO post (title, post_Body, img_Url, category_id, deleted, scheduled) VALUES ('New Post', 'Content','new URL',1,false,false); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag'); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag2'); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag3'); "
        ).executeUpdate();
    }
}
