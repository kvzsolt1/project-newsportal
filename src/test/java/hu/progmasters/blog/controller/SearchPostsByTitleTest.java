package hu.progmasters.blog.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class SearchPostsByTitleTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EntityManager entityManager;

    @TestConfiguration
    public class SecurityTestConfig {

        @Bean
        public UserDetailsService userDetailsService() {
            UserDetails user = User.withDefaultPasswordEncoder()
                    .username("testUser")
                    .password("$2a$10$CLenYEz4sBADpt4CD6tiOeXyn4dWPMFf3KNK/vrtM1u0H8PY5IgPe")
                    .roles("USER")
                    .build();
            return new InMemoryUserDetailsManager(user);
        }
    }
    @Test
    void test_searchByTitle() throws Exception {
        TestPost();
        mockMvc.perform(get("/api/public/posts/search-by-title?title=new")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title", is("New Post4")))
                .andExpect(jsonPath("$[1].title", is("New Post3")))
                .andExpect(jsonPath("$[2].title", is("New Post2")))
                .andExpect(jsonPath("$[3].title", is("New Post")));
    }
    @Test
    void test_searchByTitleEmptyTitle() throws Exception {
        TestPost();
        mockMvc.perform(get("/api/public/posts/search-by-title?title=")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title", is("New Post4")))
                .andExpect(jsonPath("$[1].title", is("New Post3")))
                .andExpect(jsonPath("$[2].title", is("New Post2")))
                .andExpect(jsonPath("$[3].title", is("New Post")));
    }
    @Test
    void test_searchByTitleNotExist() throws Exception {
        TestPost();
        mockMvc.perform(get("/api/public/posts/search-by-title?title=aladin")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    private void TestPost() {
        entityManager.createNativeQuery(
                "INSERT INTO category (category_name) VALUES ('Test Category 1'); " +
                        "INSERT INTO post (title, post_Body, img_Url, deleted, category_id, scheduled ) VALUES ('New Post', 'Content','new URL',false,1, false); " +
                        "INSERT INTO post (title, post_Body, img_Url, deleted, category_id, scheduled) VALUES ('New Post2', 'Content2','new URL2',false,1, false); " +
                        "INSERT INTO post (title, post_Body, img_Url, deleted, category_id, scheduled) VALUES ('New Post3', 'Content3','new URL3',false, 1, false); " +
                        "INSERT INTO post (title, post_Body, img_Url, deleted, category_id, scheduled) VALUES ('New Post4', 'Content4','new URL4',false,1, false); "

        ).executeUpdate();
    }
}

