package hu.progmasters.blog.controller;

import hu.progmasters.blog.domain.Comment;
import hu.progmasters.blog.domain.PostTag;
import hu.progmasters.blog.service.TagsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class TagsEditingTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EntityManager entityManager;
    @Autowired
    private TagsService tagsService;

    @TestConfiguration
    public class SecurityTestConfig {

        @Bean
        public UserDetailsService userDetailsService() {
            UserDetails user = User.withDefaultPasswordEncoder()
                    .username("testUser")
                    .password("$2a$10$CLenYEz4sBADpt4CD6tiOeXyn4dWPMFf3KNK/vrtM1u0H8PY5IgPe")
                    .roles("USER")
                    .build();
            return new InMemoryUserDetailsManager(user);
        }
    }

    @Test
    void test_tagsNotExists() throws Exception {
        TestPost();
        String inputCommand = "{\n" +
                "    \"tagsName\": \"Edited Tag\" \n" +
                "}";
        mockMvc.perform(put("/api/tags/3")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is("NOTFOUNDPOSTTAG_ERROR")))
                .andExpect(jsonPath("$.error", is("no tag found with id")));
    }

    @Test
    void test_tagEditSuccessful() throws Exception {
        TestPost();
        PostTag tag = entityManager.find(PostTag.class,1L);
        String inputCommand = "{\n" +
                "    \"tagsName\": \"Edited Tag\" \n" +
                "}";
        assertTrue(tag.getTagsName().equals("New tag"));
        mockMvc.perform(put("/api/tags/1")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isOk());
        assertTrue(tag.getTagsName().equals("Edited Tag"));
    }

    @Test
    void test_tagsNameIsEmpty() throws Exception {
        TestPost();
        PostTag tag = entityManager.find(PostTag.class,1L);
        String inputCommand = "{\n" +
                "    \"tagsName\": \"\" \n" +
                "}";
        assertTrue(tag.getTagsName().equals("New tag"));
        mockMvc.perform(put("/api/tags/1")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("tagsName")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Tag cannot be empty")));
    }

    @Test
    void test_tagsNameIsNull() throws Exception {
        TestPost();
        PostTag tag = entityManager.find(PostTag.class,1L);
        String inputCommand = "{\n" +
                "}";
        assertTrue(tag.getTagsName().equals("New tag"));
        mockMvc.perform(put("/api/tags/1")
                        .with(user("testUser").password("testPassword").roles("USER"))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(inputCommand))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors[0].field", is("tagsName")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Tag cannot be empty")));
    }

    private void TestPost() {
        entityManager.createNativeQuery(
                "INSERT INTO category (category_name) VALUES ('Test Category 1'); " +
                        "INSERT INTO post (title, post_Body, img_Url, category_id, deleted, scheduled) VALUES ('New Post', 'Content','new URL',1,false,false); " +
                        "INSERT INTO post_tag ( tags_name ) VALUES ('New tag'); "
        ).executeUpdate();
    }
}
