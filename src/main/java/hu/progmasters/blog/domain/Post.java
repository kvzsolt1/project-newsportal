/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package hu.progmasters.blog.domain;

import hu.progmasters.blog.domain.Comment;
import hu.progmasters.blog.domain.PostCategory;
import hu.progmasters.blog.domain.PostTag;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(name = "post")
@Data
@NoArgsConstructor
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @Column(name = "post_body", columnDefinition = "TEXT")
    private String postBody;
    private String imgUrl;
    private LocalDateTime createdAt;
    private boolean deleted;
    private boolean scheduled;

    @ManyToOne
    private PostCategory category;

    @OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
    @OrderBy(value = "createdAt desc")
    private List<Comment> comments;

    @ElementCollection
    private List<String> imageUrls = new ArrayList<>();
    @ManyToMany(mappedBy = "posts")
    private List<PostTag> postTags = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

//    Későbbi fejlesztés
//    @ManyToMany(mappedBy = "favoritePosts")
//    private Set<Account> favoritedBy;
}
