package hu.progmasters.blog.domain.enums;

public enum AccountRole {

    ROLE_USER, ROLE_AUTHOR, ROLE_ADMIN
}
