package hu.progmasters.blog.controller;


import hu.progmasters.blog.dto.category.CategoryCreateCommand;
import hu.progmasters.blog.dto.category.CategoryListItem;
import hu.progmasters.blog.service.CategoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/public/categories")
@AllArgsConstructor
@Slf4j
public class CategoryController {

    private CategoryService categoryService;

    @PostMapping
    public ResponseEntity createCategory(@Valid @RequestBody CategoryCreateCommand categoryCreateCommand) {
        categoryService.addCategory(categoryCreateCommand);
        log.info("Http request, POST /api/categories Category added: " + categoryCreateCommand.getCategoryName());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<CategoryListItem>> getCategories() {
        log.info("Http request, GET /api/categories Category list all");
        return new ResponseEntity<>(categoryService.getCategoryListItems(), HttpStatus.OK);
    }
}
