package hu.progmasters.blog.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
@RequestMapping(path = "/api/admin")
public class AdminController {

    @Secured("ROLE_ADMIN")
    @PostMapping("/test")
    public ResponseEntity testRole() {

        return new ResponseEntity<>(HttpStatus.OK);
    }
    //TODO: implement admin features
}
