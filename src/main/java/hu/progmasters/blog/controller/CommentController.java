/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package hu.progmasters.blog.controller;

import hu.progmasters.blog.dto.comment.CommentDetails;
import hu.progmasters.blog.dto.comment.CommentEditFormData;
import hu.progmasters.blog.dto.comment.CommentFormData;
import hu.progmasters.blog.service.CommentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/public/comments")
@AllArgsConstructor
@Slf4j
public class CommentController {

    private CommentService commentService;

    @PostMapping
    public ResponseEntity createComment(@Valid @RequestBody CommentFormData commentFormData) {
        commentService.createComment(commentFormData);
        log.info("Http request, POST /api/comments  Comment created");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{commentId}")
    public ResponseEntity editComment(@PathVariable("commentId") Long id,
                                      @Valid @RequestBody CommentEditFormData commentFormData) {
        commentService.editComment(id, commentFormData);
        log.info("Http request, PUT /api/comments/{commentId}" + id + " comment edited");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{commentId}")
    public ResponseEntity<Void> deleteComment(@PathVariable("commentId") Long id) {
        commentService.deleteComment(id);
        log.info("Http request, DELETE /api/comments/{commentId}" + id + " comment deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{postId}")
    public ResponseEntity<List<CommentDetails>> getComments(@PathVariable("postId") Long id) {
        log.info("Http request, GET /api/comments/{postId} " + id + " comment list by postid");
        return new ResponseEntity<>(commentService.getCommentsList(id), HttpStatus.OK);
    }
}
