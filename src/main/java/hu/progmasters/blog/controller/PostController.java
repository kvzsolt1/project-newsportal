/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package hu.progmasters.blog.controller;

import com.itextpdf.text.DocumentException;
import hu.progmasters.blog.config.PdfUtilsConfig;
import hu.progmasters.blog.dto.google.*;
import hu.progmasters.blog.dto.post.*;
import hu.progmasters.blog.service.EmailService;
import hu.progmasters.blog.service.PostService;
//import hu.progmasters.blog.validator.PostFormDataValidator;
import hu.progmasters.blog.service.google.TextToSpeechService;
import hu.progmasters.blog.service.google.TranslateApiService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/public/posts")
@AllArgsConstructor
@Slf4j
public class PostController {

    private PostService postService;
    private EmailService emailService;
    private TranslateApiService translateApiService;
    private TextToSpeechService textToSpeechService;

    @PostMapping
    public ResponseEntity createPost(@Valid @RequestBody PostFormData postFormData) throws DocumentException, IOException {
        postService.createPost(postFormData);
//        emailService.sendEmailWithAttachment(postFormData.getTitle());
        log.info("Http request, POST /api/posts/ Post created");
//        emailService.sendEmail("blogprogmasters@gmail.com", "Post created", "Http request, POST /api/posts/ Post created");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PostMapping("/scheduled/{hour},{minute}")
    public ResponseEntity createPostScheduled(@Valid @RequestBody PostFormData postFormData, @PathVariable("hour") int hour, @PathVariable("minute") int minute) {
        postService.createPostScheduled(postFormData, hour, minute);
        log.info("Http request, POST /api/posts/scheduled/{hour},{minute} " + hour + ":" + minute + " Scheduled post scheduled");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/scheduled/{postId}")
    public ResponseEntity editScheduledPost(@PathVariable("postId") Long id,
                                            @Valid @RequestBody PostFormData postFormData) {
        postService.editingScheduledPost(id, postFormData);
        log.info("Http request, PUT /api/posts/scheduled/{postId}" + id + " Scheduled post edited");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/scheduled/deleted/{postId}")
    public ResponseEntity deleteSheduledPost(@PathVariable("postId") Long id) {
        postService.deleteScheduledPost(id);
        log.info("Http request, PUT /api/posts/scheduled/deleted/{postId}" + id + " Scheduled post deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<PostListItem>> getPostList() {
        log.info("Http request, GET /api/posts/  All post listed");
        return new ResponseEntity<>(postService.getPostListItems(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostDetails> getPost(@PathVariable("id") Long id) {
        PostDetails postDetails = postService.getPostDetailsById(id);
        log.info("Http request, GET /api/posts/{postId}" + id + " Post find by id");
        return new ResponseEntity<>(postDetails, HttpStatus.OK);
    }

    @PutMapping("/delete/{postId}")
    public ResponseEntity deletePost(@PathVariable("postId") Long id) {
        postService.deletePost(id);
        log.info("Http request, PUT /api/posts/delete/{postId}" + id + " Post deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/restore/{postId}")
    public ResponseEntity restorationPost(@PathVariable("postId") Long id) {
        postService.restorationPost(id);
        log.info("Http request, PUT /api/posts/restore/{postId}" + id + " Deleted post restored");
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @PutMapping("/edit/{postId}")
    public ResponseEntity<PostDetails> editPost(@PathVariable("postId") Long id,
                                                @Valid @RequestBody PostFormData postFormData) {
        PostDetails edited = postService.editPost(id, postFormData);
        log.info("Http request, PUT /api/posts/edit/{postId} body: " + postFormData.toString() + " post edited id: " + id);
        return new ResponseEntity<>(edited, HttpStatus.OK);
    }
    @PutMapping("/uploadImages/{postId}")
    public ResponseEntity<Void> uploadImages(@PathVariable("postId") Long id,
                                         @ModelAttribute @Valid PostImage postImage){
        postService.uploadImage(id,postImage);
        log.info("Http request, PUT /api/posts/uploadImages/{postId}, image(s) uploaded to post id: " + id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/pdf/{postId}")
    public ResponseEntity<byte[]> exportPdf(@PathVariable("postId") Long id) throws IOException, DocumentException {
        List<Map<String, Object>> queryResults = postService.executeQuery(id);
        ByteArrayOutputStream pdfStream = PdfUtilsConfig.generatePdfStream(queryResults);
        log.info("Http request, GET /api/posts/pdf/{postId}, pdf created to post id: " + id);
        return new ResponseEntity<>(pdfStream.toByteArray(), HttpStatus.OK);
    }

    @PostMapping("/translate")
    public ResponseEntity<TranslateRes> translateLanguage(@RequestBody TranslateReq request) throws IOException {
        TranslateRes response = translateApiService.translate(request);
        log.info("Http request, POST /api/posts/translate, Post translated");
        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @PostMapping("/synthesize")
    public ResponseEntity<byte[]> synthesizeSpeech(@RequestBody TextToSpeechReq request) throws IOException {
            byte[] audioContent = textToSpeechService.synthesizeText(request);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            log.info("Http request, POST /api/posts/synthesize, Post synthesize");
            return new ResponseEntity<>(audioContent, headers, HttpStatus.OK);
    }
    @GetMapping("/search-by-title")
    public ResponseEntity<List<PostDetailsShortened>> searchByTitle(@RequestParam String title) {
        List<PostDetailsShortened> posts = postService.searchPostsByTitle(title);
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }
}
