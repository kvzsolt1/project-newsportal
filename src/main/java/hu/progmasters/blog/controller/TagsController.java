package hu.progmasters.blog.controller;

import hu.progmasters.blog.dto.tags.PostTagListItem;
import hu.progmasters.blog.dto.tags.TagsFromData;
import hu.progmasters.blog.service.TagsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
@AllArgsConstructor
@Slf4j
public class TagsController {

    private TagsService tagsService;

    @PostMapping("/{postId}")
    public ResponseEntity createTags(@PathVariable("postId") Long id,
                                     @Valid @RequestBody TagsFromData... tagsFromData) {
        tagsService.createTags(id, tagsFromData);
        log.info("Http request, POST /api/tags/postId: " + id + " Tags created");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/{PostTagId}")
    public ResponseEntity<Void> deletePostTag(@PathVariable("PostTagId") Long id) {
        tagsService.deleteTags(id);
        log.info("Http request, PUT /api/tags/{postTagId}" + id + " tags deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{postTagId}")
    public ResponseEntity editPostTag(@PathVariable("postTagId") Long id,
                                      @Valid @RequestBody TagsFromData tagsFromData) {
        tagsService.editingTag(id, tagsFromData);
        log.info("Http request, PUT /api/tags/{postTagId}" + id + " Tag edited");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<PostTagListItem>> getPostTagList() {
        log.info("Http request, GET /api/tags/  All tags listed");
        return new ResponseEntity<>(tagsService.getPostTagListItems(), HttpStatus.OK);
    }
}
