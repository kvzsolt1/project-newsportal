package hu.progmasters.blog.controller;

import hu.progmasters.blog.dto.*;
import hu.progmasters.blog.dto.security.RegistrationFormData;
import hu.progmasters.blog.dto.security.RegistrationInfo;
import hu.progmasters.blog.dto.security.RegistrationNewPasswordFormData;
import hu.progmasters.blog.service.AccountService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountNotFoundException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/public/user")
@AllArgsConstructor
@Slf4j
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/registration")
    public ResponseEntity<RegistrationInfo> registerAccount(@Valid @RequestBody RegistrationFormData registrationFormData) {
        RegistrationInfo registrationInfo = accountService.registerAccount(registrationFormData);
        log.info("Http request, POST /api/user/registration New account added");
        return new ResponseEntity(registrationInfo, HttpStatus.CREATED);
    }

    @PostMapping("/request")
    public ResponseEntity<RegistrationInfo> requestPasswordReset(@RequestParam String email) throws AccountNotFoundException {
        RegistrationInfo registrationInfo = accountService.resetPasswordRequest(email);
        log.info("Http request, POST /api/user/request Password reset request");
        return new ResponseEntity<>(registrationInfo, HttpStatus.OK);
    }

    @PostMapping("/reset")
    public ResponseEntity<RegistrationInfo> resetPassword(@RequestParam String token, @Valid @RequestBody RegistrationNewPasswordFormData newPassword) throws AccountNotFoundException {
        RegistrationInfo registrationInfo = accountService.resetPassword(token, newPassword.getPassword());
        log.info("Http request, POST /api/user/reset Password reset completed");
        return new ResponseEntity<>(registrationInfo, HttpStatus.OK);
    }
    @GetMapping("/profile/{userId}")
    public ResponseEntity<UserInfo> getUserProfile(@PathVariable("userId") Long id) {
        UserInfo userInfo = accountService.getUserInfo(id);
        log.info("Http request, GET /api/user/profile/{userId}" + id + " Profil find by id");
        return new ResponseEntity<>(userInfo, HttpStatus.OK);
    }

    @PutMapping("/profile/update/{userId}")
    public ResponseEntity updateAccountInfo(@PathVariable("userId") Long id,
                                            @Valid @RequestBody AccountEditFormData accountEditFormData) {
        accountService.editAccount(id, accountEditFormData);
        log.info("Http request, PUT /api/user/update/{userId}" + id + " Profil update by id");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/profile/newsletter/{userId}")
    public ResponseEntity subscribeNewsletter(@PathVariable("userId") Long id,
                                              @Valid @RequestBody SubscriptionUpdate subscriptionUpdate) {
        accountService.editSubscription(id, subscriptionUpdate);
        log.info("Http request, PUT /api/user/profile/newsletter/{userId}" + id + " Profil newsLetter added");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/profile/premium/{userId}")
    public ResponseEntity updatePremium(@PathVariable("userId") Long id,
                                        @Valid @RequestBody PremiumUpdate premiumUpdate) {
        accountService.editPremium(id, premiumUpdate);
        log.info("Http request, PUT /api/user/profile/premium/{userId}" + id + " Profil premium update");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
