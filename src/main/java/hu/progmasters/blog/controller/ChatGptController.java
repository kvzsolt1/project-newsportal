package hu.progmasters.blog.controller;

import hu.progmasters.blog.service.ChatGptService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/public/gpt")
@AllArgsConstructor
@Slf4j
public class ChatGptController {
    @Autowired
    private ChatGptService chatGptService;

    @PostMapping("/chat")
    public String chat(@RequestBody String message) {
        log.info("Http request, POST /api/gpt/chat ChatGpt answer");
        return chatGptService.responseContent(message);
    }

    @PostMapping("/generate")
    public String generate(@RequestBody String prompt) {
        log.info("Http request, POST /api/gpt/generate ChatGpt generate picture");
        return chatGptService.generatePicture(prompt);
    }

    @PostMapping("/generate/{postId}")
    public String generateByTitle(@PathVariable("postId") Long id) {
        log.info("Http request, POST /api/gpt/generate/postId" + id + "ChatGpt generate picture by post title");
        return chatGptService.generatePictureByTitle(id);
    }

    @PostMapping("/moderate")
    public String moderate() {
        log.info("Http request, POST /api/gpt/moderate ChatGpt analize content");
        return chatGptService.getModerateResponse();
    }

}
