package hu.progmasters.blog.controller;

import hu.progmasters.blog.dto.paypal.CompleteOrder;
import hu.progmasters.blog.dto.paypal.PaymentOrder;
import hu.progmasters.blog.service.EmailService;
import hu.progmasters.blog.service.PaypalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@Slf4j
@RequestMapping(value = "/api/public/paypal")
@CrossOrigin(origins = "https://api-m.sandbox.paypal.com/v2/checkout/orders")
public class PaypalController {
    @Autowired
    private PaypalService paypalService;
    @Autowired
    private EmailService emailService;

    @PostMapping(value = "/init")
    public PaymentOrder createPayment(@RequestParam("sum") BigDecimal sum, @RequestParam("dev") String dev) {
        log.info("Http request, POST /api/paypal/init Paypal request send");
        return paypalService.createPayment(sum, dev);
    }

    @PostMapping(value = "/capture")
    public CompleteOrder completePayment(@RequestParam("token") String token) {
        log.info("Http request, POST /api/paypal/capture Paypal payd confirmed");
        return paypalService.completePayment(token);
    }


    @GetMapping("/payment-success")
    public String handlePaymentSuccess(@RequestParam("token") String token) {
        String captureLink = "http://localhost:8080/api/public/paypal/capture?token=" + token;
        String htmlContent = "<html><body><h1>Payment successful!</h1>"
                + "<p>Token: <a href='" + captureLink + "'>" + captureLink + "</a></p>"
                + "</body></html>";
        emailService.sendEmail("blogprogmasters@gmail.com", "Payment ", "Payment succesful");
        log.info("Http request, GET /api/paypal/payment-success Paypal payment succesful");
        return htmlContent;
    }
    @GetMapping("/payment-cancel")
    public String handlePaymentCancel() {
        String htmlContent = "<html><body><h1>Payment cancel!</h1></body></html>";
        emailService.sendEmail("blogprogmasters@gmail.com", "Payment ", "Payment canceled");
        log.info("Http request, GET /api/paypal/payment-cancel Paypal payment cancel");
        return htmlContent;
    }

}