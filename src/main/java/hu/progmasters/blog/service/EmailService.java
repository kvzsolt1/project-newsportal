package hu.progmasters.blog.service;

import com.itextpdf.text.DocumentException;
import hu.progmasters.blog.config.PdfUtilsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


@Component
@Service
@Transactional
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private PostService postService;

    public void sendEmail(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    public void sendEmailWithAttachment(String title) throws IOException, DocumentException {
        ByteArrayOutputStream pdfStream = PdfUtilsConfig.generatePdfStreamTwo();
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo("blogprogmasters@gmail.com");
            helper.setSubject("Megvásárolt valami");
            helper.setText("Köszönjük, hogy megvetted a valamit");
            helper.addAttachment("vásárlási bizonylat.pdf", new ByteArrayResource(pdfStream.toByteArray()));
            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
