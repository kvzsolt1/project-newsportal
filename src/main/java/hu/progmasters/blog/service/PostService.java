/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package hu.progmasters.blog.service;

import hu.progmasters.blog.domain.Account;
import hu.progmasters.blog.domain.Post;
import hu.progmasters.blog.dto.post.*;
import hu.progmasters.blog.domain.PostCategory;
import hu.progmasters.blog.domain.PostTag;
import hu.progmasters.blog.exception.DateEarlierThanTheCurrentOneException;
import hu.progmasters.blog.exception.NotFoundPostException;
import hu.progmasters.blog.repository.AccountRepository;
import hu.progmasters.blog.repository.PostRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class PostService {
    private final AccountRepository accountRepository;

    private PostRepository postRepository;
    private CategoryService categoryService;
    private final ModelMapper modelMapper;
    private final ImageUploadService imageUploadService;
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public Post createPost(PostFormData postFormData) {
        Post newPost = modelMapper.map(postFormData, Post.class);

        PostCategory categoryForThePost = categoryService.getCategoryByName(postFormData.getCategory());
        newPost.setCategory(categoryForThePost);
        newPost.setCreatedAt(LocalDateTime.now());
        newPost.setAccount(findAccountById(postFormData.getAccountId()));

        postRepository.save(newPost);
        return newPost;
    }

    public void uploadImage(Long id, PostImage postImage){
        List<String> urls = imageUploadService.uploadImages(postImage.getImages());
        Post postToUploadImages = findPostById(id);
        postToUploadImages.setImageUrls(urls);
    }

    public void createPostScheduled(PostFormData postFormData, int hour, int minute) {
        LocalTime desiredTime = LocalTime.of(hour, minute);
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime scheduledTime = LocalDateTime.of(now.toLocalDate(), desiredTime);

        if (now.isAfter(scheduledTime)) {
            throw new DateEarlierThanTheCurrentOneException();
        }

        Post newPost = createPost(postFormData);
        newPost.setScheduled(true);
        newPost.setDeleted(true);
        postRepository.save(newPost);

        long delay = ChronoUnit.SECONDS.between(now, scheduledTime);

        Runnable task = () -> {
            Post sheduledPost = findPostById(newPost.getId());
            if (sheduledPost.isScheduled()) {
                sheduledPost.setCreatedAt(LocalDateTime.now());
                sheduledPost.setDeleted(false);
                postRepository.save(sheduledPost);
                log.info("Scheduled post created ", LocalDateTime.now());
            } else {
                postRepository.delete(sheduledPost);
            }
        };
        scheduler.schedule(task, delay, TimeUnit.SECONDS);
    }

    public void editingScheduledPost(Long id, PostFormData postFormData) {
        Post editedPost = findPostById(id);
        modelMapper.map(postFormData, editedPost);
        postRepository.save(editedPost);
    }

    public void deleteScheduledPost(Long id) {
        Post deletedPost = findPostById(id);
        deletedPost.setDeleted(true);
        deletedPost.setScheduled(false);
    }

    public List<PostListItem> getPostListItems() {
        List<Post> posts = postRepository.findAllByOrderByCreatedAtDesc();
        return posts.stream()
                .filter(post -> !post.isDeleted())
                .map(this::postToPostListItem)
                .collect(Collectors.toList());
    }

    private PostListItem postToPostListItem(Post post) {
        PostListItem postListItem = modelMapper.map(post, PostListItem.class);
        postListItem.setPostBodyShortened(shorter(post.getPostBody()));
        postListItem.setCategoryName(post.getCategory().getCategoryName());
        for (PostTag postTag : post.getPostTags()) {
            postListItem.getTagsName().add(postTag.getTagsName());
        }
        postListItem.setNumberOfComments(post.getComments().size());
        return postListItem;
    }

    private String shorter(String postBody) {
        return Stream.of(postBody)
                .map(string -> string.substring(0, Math.min(200, string.length())))
                .map(string -> string.substring(0, string.contains(" ") && postBody.length() > 205 ? string.lastIndexOf(" ") : string.length()))
                .map(string -> string.equals(postBody) ? string : string.concat("..."))
                .collect(Collectors.joining());
    }

    public List<Map<String, Object>> executeQuery(Long id) {
        Post post = findPostById(id);
        List<Map<String, Object>> queryResults = new ArrayList<>();
        Map<String, Object> commentData = new HashMap<>();
        commentData.put("postBody", post.getPostBody());
        commentData.put("title", post.getTitle());
        queryResults.add(commentData);
        return queryResults;
    }

    public List<Map<String, Object>> execute(String title) {
        List<Map<String, Object>> queryResults = new ArrayList<>();
        Map<String, Object> commentData = new HashMap<>();
        commentData.put("Vásárlási bizonylat", title);
        commentData.put("title", "Köszönjük, hogy köszi :)");
        queryResults.add(commentData);
        return queryResults;
    }

    public PostDetails getPostDetailsById(Long id) {
        Post post = findPostById(id);
        PostCategory category = post.getCategory();
        PostDetails postDetails = modelMapper.map(post, PostDetails.class);
        postDetails.setCategoryName(category.getCategoryName());
        return postDetails;
    }

    public Post findPostById(Long postId) {
        return postRepository.findById(postId).orElseThrow(NotFoundPostException::new);
    }

    public void deletePost(Long id) {
        Post deletedPost = findPostById(id);
        deletedPost.setDeleted(true);
    }

    public void restorationPost(Long id) {
        Post restorationPost = findPostById(id);
        restorationPost.setDeleted(false);
    }

    public PostDetails editPost(Long id, PostFormData postFormData) {
        Post editPost = findPostById(id);
        modelMapper.map(postFormData, editPost);
        return modelMapper.map(editPost, PostDetails.class);
    }

    private Account findAccountById(Long accountId) {
        return accountRepository.findById(accountId).orElseThrow(IllegalArgumentException::new);
    }
    public List<PostDetailsShortened> searchPostsByTitle(String title) {
        return postRepository.searchPostByTitleContainsIgnoreCaseOrderByIdDesc(title)
                .stream()
                .map(post -> modelMapper.map(post, PostDetailsShortened.class))
                .collect(Collectors.toList());
    }

}
