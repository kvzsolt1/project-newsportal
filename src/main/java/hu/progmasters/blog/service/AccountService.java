package hu.progmasters.blog.service;

import hu.progmasters.blog.repository.PostRepository;
import hu.progmasters.blog.security.JwtTokenUtil;
import hu.progmasters.blog.domain.Account;
import hu.progmasters.blog.domain.Post;
import hu.progmasters.blog.domain.enums.AccountRole;
import hu.progmasters.blog.dto.*;
import hu.progmasters.blog.dto.post.PostDetailsShortened;
import hu.progmasters.blog.dto.security.RegistrationFormData;
import hu.progmasters.blog.dto.security.RegistrationInfo;
import hu.progmasters.blog.exception.FieldNotAvailableException;
import hu.progmasters.blog.repository.AccountRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import javax.security.auth.login.AccountNotFoundException;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class AccountService {

    private final ModelMapper modelMapper;

    private final AccountRepository accountRepository;

    private final PasswordEncoder passwordEncoder;

    private final PostRepository postRepository;

    private final EmailService emailService;

    private final JwtTokenUtil jwtTokenUtil;


    public RegistrationInfo registerAccount(RegistrationFormData data) {
        checkFieldAvailability(data);
        Account account = modelMapper.map(data, Account.class);
        account.setPassword(passwordEncoder.encode(data.getPassword()));
        account.setRole(AccountRole.ROLE_USER);
        account.setPremium(false);
        account.setNewsletter(false);
        accountRepository.save(account);
        return new RegistrationInfo("Successful registration.");
    }

    public UserInfo getUserInfo(Long id) {
        Account userAccount = accountRepository.getById(id);
        UserInfo userInfo = modelMapper.map(userAccount, UserInfo.class);
        List<Post> postsByUser = postRepository.findPostsByAccount_Id(userInfo.getId());
        List<PostDetailsShortened> shortenedPosts = postsByUser.stream()
                .map(post -> modelMapper.map(post, PostDetailsShortened.class))
                .collect(Collectors.toList());
        userInfo.setWrittenPosts(shortenedPosts);
        return userInfo;
    }

    public void editAccount(Long id, AccountEditFormData accountEditFormData) {
        Account editedAccount = findAccountById(id);
        modelMapper.map(accountEditFormData, editedAccount);
        accountRepository.save(editedAccount);
    }

    public void editSubscription(Long id, SubscriptionUpdate subscriptionUpdate) {
        Account editedAccount = findAccountById(id);
        modelMapper.map(subscriptionUpdate, editedAccount);
        accountRepository.save(editedAccount);
    }

    public void editPremium(Long id, PremiumUpdate premiumUpdate) {
        Account editedAccount = findAccountById(id);
        modelMapper.map(premiumUpdate, editedAccount);
        accountRepository.save(editedAccount);
    }

    private void checkFieldAvailability(RegistrationFormData data) {
        if (accountRepository.existsByUsername(data.getUsername())) {
            throw new FieldNotAvailableException("Account with " + data.getUsername() + " already exist.");
        }
        if (accountRepository.existsByEmail(data.getEmail())) {
            throw new FieldNotAvailableException("Account with " + data.getEmail() + " already exist.");
        }
    }

    private Account findAccountById(Long id) {
        return accountRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public Account findByEmail(String email) {
        return accountRepository.findByEmail(email).orElseThrow(RuntimeException::new);
    }

    public RegistrationInfo resetPasswordRequest(String email) throws AccountNotFoundException {
        Account account = findByEmail(email);
        if (account == null) {
            throw new AccountNotFoundException("Account The specified email address cannot be found: " + email);
        }

        String token = jwtTokenUtil.generatePasswordResetToken(account);

        String resetLink = "http://localhost:8080/api/public/user/reset?token=" + token;
        String emailBody = "Kattintson ide a jelszó visszaállításához: " + resetLink;
        emailService.sendEmail(email, "Jelszó visszaállítása", emailBody);
        return new RegistrationInfo("A jelszó visszaállítási linket elküldtük az e-mail címére.");
    }

    public RegistrationInfo resetPassword(String token, String newPassword) throws AccountNotFoundException {
        if (!jwtTokenUtil.validateResetToken(token)) {
            log.error("Token is expired");
        }
        String email = jwtTokenUtil.getEmailFromPasswordResetToken(token);
        Account account = findByEmail(email);
        if (account == null) {
            throw new AccountNotFoundException("Account The specified email address cannot be found: a token mókolva lett");
        }
        account.setPassword(passwordEncoder.encode(newPassword));
        accountRepository.save(account);
        emailService.sendEmail(email,"Jelszó visszaállítás","Jelszó sikeresen frissítve :)");
        return new RegistrationInfo("A jelszó sikeresen frissítve");
    }
}
