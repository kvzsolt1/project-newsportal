package hu.progmasters.blog.service.google;

import hu.progmasters.blog.dto.google.TranslateReq;
import hu.progmasters.blog.dto.google.TranslateRes;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Service
public class TranslateApiService {

    private final GoogleAuthService googleAuthService;
    private static final String API_URL = "https://translation.googleapis.com/language/translate/v2";

    public TranslateApiService(GoogleAuthService googleAuthService) {
        this.googleAuthService = googleAuthService;
    }

    public TranslateRes translate(TranslateReq translateRequest) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add("Authorization", "Bearer " + googleAuthService.authGoogle().getAccessToken().getTokenValue());

        Map<String, String> stringStringMap = new HashMap<>();
        stringStringMap.put("q", translateRequest.getQ());
        stringStringMap.put("target", translateRequest.getTarget());
        HttpEntity<Map<String, String>> entity = new HttpEntity<>(stringStringMap, httpHeaders);

        return restTemplate.postForEntity(API_URL, entity, TranslateRes.class).getBody();
    }


}
