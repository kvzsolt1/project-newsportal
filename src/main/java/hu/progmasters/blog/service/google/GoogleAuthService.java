package hu.progmasters.blog.service.google;

import com.google.auth.oauth2.GoogleCredentials;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Objects;


@Service
public class GoogleAuthService {

    public GoogleCredentials authGoogle() throws IOException {
        GoogleCredentials credentials = GoogleCredentials.fromStream(Files.newInputStream(Paths.get(Objects.requireNonNull(Dotenv.load().get("GOOGLE_APPLICATION_CREDENTIALS")))))
                .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));

        credentials.refreshIfExpired();
        return credentials;
    }
}


