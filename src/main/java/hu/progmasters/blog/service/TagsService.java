package hu.progmasters.blog.service;

import hu.progmasters.blog.domain.Post;
import hu.progmasters.blog.domain.PostTag;
import hu.progmasters.blog.dto.tags.PostTagListItem;
import hu.progmasters.blog.dto.tags.TagsFromData;
import hu.progmasters.blog.exception.NotFoundPostTagException;
import hu.progmasters.blog.repository.TagsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class TagsService {

    private TagsRepository tagsRepository;
    private PostService postService;
    private ModelMapper modelMapper;

    public void createTags(Long id, TagsFromData[] tagsFromData) {
        Post post = postService.findPostById(id);
        for (TagsFromData tags : tagsFromData) {
            PostTag checkedTag = tagsRepository.findByTagByNameByPostId(tags.getTagsName(), id).orElse(null);
            if (checkedTag == null) {
                PostTag tag = modelMapper.map(tags, PostTag.class);
                tagsRepository.save(tag);
                tag.getPosts().add(post);
                post.getPostTags().add(tag);
            }
        }
    }

    public void deleteTags(Long id) {
        PostTag tag = findPostTagById(id);
        tagsRepository.delete(tag);
    }

    public void editingTag(Long id, TagsFromData tagsFromData) {
        PostTag editedTag = findPostTagById(id);
        modelMapper.map(tagsFromData, editedTag);
        tagsRepository.save(editedTag);
    }

    public PostTag findPostTagById(Long id) {
        return tagsRepository.findById(id).orElseThrow(NotFoundPostTagException::new);
    }

    public List<PostTagListItem> getPostTagListItems() {
        return tagsRepository.findAllOrderBy();
    }
}
