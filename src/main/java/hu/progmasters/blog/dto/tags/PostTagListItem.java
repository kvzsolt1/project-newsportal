package hu.progmasters.blog.dto.tags;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostTagListItem {

    private String tagsName;
    private int numberOfPostsWithSuchTag;
}
