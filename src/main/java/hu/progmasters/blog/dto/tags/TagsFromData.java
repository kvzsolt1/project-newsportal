package hu.progmasters.blog.dto.tags;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagsFromData {

    @NotBlank(message = "Tag cannot be empty")
    private String tagsName;

}
