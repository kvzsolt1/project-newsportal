package hu.progmasters.blog.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountEditFormData {

    private String realName;
    private LocalDate dateOfBirth;
    private String aboutMe;
    private String profileImageUrl;
}
