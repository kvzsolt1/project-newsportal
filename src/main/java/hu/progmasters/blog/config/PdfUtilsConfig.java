package hu.progmasters.blog.config;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.context.annotation.Bean;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class PdfUtilsConfig {
    @Bean
    public static ByteArrayOutputStream generatePdfStream(List<Map<String, Object>> queryResults) throws DocumentException {
        Document document = new Document();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, outputStream);
        document.open();
        // Write column names
        Map<String, Object> firstRow = queryResults.get(0);
        for (String column : firstRow.keySet()) {
            Font boldFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
            Paragraph paragraph = new Paragraph(column, boldFont);
            document.add(paragraph);
        }
        document.add(new Paragraph("\n"));
        // Write data rows
        for (Map<String, Object> row : queryResults) {
            for (Object value : row.values()) {
                Paragraph paragraph = new Paragraph(value.toString());
                document.add(paragraph);
            }
            document.add(new Paragraph("\n"));
        }
        document.close();
        return outputStream;
    }

    public static ByteArrayOutputStream generatePdfStreamTwo() throws DocumentException {
        Document document = new Document();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, outputStream);
        document.open();

        // Cím
        Font titleFont = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD | Font.UNDERLINE);
        Paragraph title = new Paragraph("Vásárlási bizonylat", titleFont);
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);

        // Megrendelő adatai
        Font headerFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
        Paragraph customerHeader = new Paragraph("Megrendelő adatai:", headerFont);
        document.add(customerHeader);

        // Account neve és e-mail címe
        addTextToParagraph(document, "  - Név: ");
        addTextToParagraph(document, "  - E-mail: ");

        // Megrendelő címe
        Paragraph addressHeader = new Paragraph("Megrendelő címe:", headerFont);
        document.add(addressHeader);

        // Account címe
        addTextToParagraph(document, "  - Cím: ");

        // Megrendelt termék
        Paragraph productHeader = new Paragraph("Megrendelt termék:", headerFont);
        document.add(productHeader);

        // Packet neve
        addTextToParagraph(document, "  - Termék neve: ");

        // Teljes ár (jobbra zárt)
        Font totalPriceFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
        Paragraph totalPrice = new Paragraph("Teljes ár: " + calculateTotalPrice(), totalPriceFont);
        totalPrice.setAlignment(Element.ALIGN_RIGHT);
        document.add(totalPrice);

        // LocalDate.now()
        Font dateFont = new Font(Font.FontFamily.HELVETICA, 12);
        Paragraph date = new Paragraph("Dátum: " + LocalDate.now().toString(), dateFont);
        date.setAlignment(Element.ALIGN_RIGHT);
        document.add(date);

        document.close();
        return outputStream;
    }

    private static void addTextToParagraph(Document document, String text) throws DocumentException {
        Font regularFont = new Font(Font.FontFamily.HELVETICA, 12);
        Paragraph paragraph = new Paragraph(text, regularFont);
        document.add(paragraph);
    }

    private static double calculateTotalPrice() {
        // Ide írd a logikát a teljes ár kiszámításához
        return 10;
    }
}
