package hu.progmasters.blog.config;

import com.paypal.core.PayPalEnvironment;
import com.paypal.core.PayPalHttpClient;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaypalConfiguration {


    @Bean
    public PayPalHttpClient paypalClient() {
        Dotenv dotenv = Dotenv.load();
        String paypalClientId = dotenv.get("PAYPAL_CLIENT_ID");
        String paypalClientSecret = dotenv.get("PAYPAL_CLIENT_SECRET");

        return new PayPalHttpClient(new PayPalEnvironment.Sandbox(paypalClientId, paypalClientSecret));
    }
}