# Project-NewsPortal

## Projekt Leírás
Az "Project-NewsPortal" egy modern hírportál alkalmazás, amely Java Spring Boot technológiával készült.

## Technológiai Követelmények
- **Java Verzió**: 9
- **Spring Boot**: 2.5.6

## Függőségek
A projekt a következő főbb függőségeket használja:
- Spring Boot Starter (Data JPA, Web, WebFlux, Validation)
- Spring Context
- PayPal SDK Checkout SDK
- MySQL Connector Java
- Lombok
- ModelMapper
- iTextPDF
- H2 Database
- Spring Boot Starter Security
- Auth0 Java JWT
- JSON Web Token (JWT)
- Java Dotenv
- Google Cloud Services (Translate, Text-to-Speech, Storage)
- Cloudinary
- Apache Tika
- SpringFox (Swagger)

## Integrált Külső Szolgáltatások
- **PayPal**: Online fizetési megoldások integrációja.
- **Google Cloud Services**:
  - **Translate**: Szövegfordítási szolgáltatás.
  - **Text-to-Speech**: Szövegből beszéd generálása.
- **Cloudinary**: Kép- és videókezelési platform.
- **iTextPDF**: PDF dokumentumok generálása és kezelése.
